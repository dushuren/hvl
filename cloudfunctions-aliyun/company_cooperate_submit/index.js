'use strict';
const db = uniCloud.database();
exports.main = async (event, context) => {
  //event为客户端上传的参数
  event.createTime = Date.now();
  const collection = db.collection('company_cooperate');
  await collection.add(event);
  return {
  	status: 1
  }
};
