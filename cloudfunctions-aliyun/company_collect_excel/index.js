'use strict';
const db = uniCloud.database()
const dbCmd = db.command
// 字符串转时间戳
function dateToMs(date) {
	var result = new Date(date).getTime();
	return result;
}
// 获取指定的今天的时间戳
function getDateMs(timeStr, dayStr) {
	if (dayStr) {
		return dateToMs(dayStr+' '+timeStr);
	}
	var curDate = new Date();
	var curYear = curDate.getFullYear(); //获取完整的年份(4位,1970-????)
	var curMonth = curDate.getMonth() + 1; //获取当前月份(0-11,0代表1月)
	var curDay = curDate.getDate(); //获取当前日(1-31)
	return dateToMs(curYear+'-'+curMonth+'-'+curDay+' '+timeStr);
}
exports.main = async (event, context) => {
	let todayStart = getDateMs('00:00:00',event.day);
	let todayEnd = getDateMs('23:59:59',event.day);
	const whereArr = {
		'company': `${event.company}` || '',
		'createTime': dbCmd.gte(todayStart).and(dbCmd.lte(todayEnd))
	};
  let collectData = await db.collection('company_collect').where(whereArr).get();
  return {
	  status:1,
	  excelData:collectData.data
  };
};
