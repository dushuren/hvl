'use strict';
const db = uniCloud.database()
const dbCmd = db.command
const diffTime = 8 * 60 * 60 * 1000;
// 获取指定的今天的时间戳
function getMsByDate(timeStr, dayStr) {
	return new Date(dayStr+' '+timeStr).getTime()-diffTime;
}
exports.main = async (event, context) => {
	const skipCount = (event.page - 1) * event.size;
	let todayStart = getMsByDate('00:00:00',event.day);
	let todayEnd = getMsByDate('23:59:59',event.day);
	const whereArr = {
		'company': `${event.company}` || '',
		'createTime': dbCmd.gte(todayStart).and(dbCmd.lte(todayEnd))
	};
	if(event.advice>0) {
		whereArr.advice = event.advice;
	}
	if(event.name) {
		whereArr.name = event.name;
	}
	if(event.department) {
		whereArr.department = event.department;
	}
	
	let collection = db.collection('company_collect').where(whereArr);
	
	const totalRes = await collection.count();
	let res = {};
	if (totalRes.total>0) {
	// const res = await collection.limit(size).get()
		res = await collection.skip(skipCount).limit(event.size).orderBy("createTime", "desc").get();
	}
	return {
		page: event.page,
		size: event.size,
		total: totalRes.total || 0,
		data: res.data || []
	};
};
