'use strict';
const db = uniCloud.database()
exports.main = async (event, context) => {
	const $ = db.command.aggregate;
	const collection = db.collection('company_collect');
	let whereMap = {
		'company': event.company
	};
	const skipCount = (event.page - 1) * event.size;
	const res = await collection
		.aggregate()
		.match(whereMap)
		.skip(skipCount)
		.limit(event.size)
		.group({
			_id: '$dayTime',
			data: $.sum(1)
		})
		.sort({
			_id: -1
		})
		.end();
	return {
		data: res.data || []
	};
};
