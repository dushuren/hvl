'use strict';
const db = uniCloud.database()
const dbCmd = db.command
exports.main = async (event, context) => {
	const whereArr = {
		'createTime': dbCmd.gt(0)
	};
  let collectData = await db.collection('company_accout').where(whereArr).get();
  return {
	  status:1,
	  excelData:collectData.data
  };
};
