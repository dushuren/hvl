import Vue from 'vue'
import App from './App'
import JsonExcel from 'vue-json-excel'
import pageHead from './components/page-head.vue'
import store from './store'

Vue.config.productionTip = false
Vue.prototype.$store = store

App.mpType = 'app'
Vue.component('downloadExcel', JsonExcel)
Vue.component('page-head', pageHead)
const app = new Vue({
	store,
    ...App
})
app.$mount()

